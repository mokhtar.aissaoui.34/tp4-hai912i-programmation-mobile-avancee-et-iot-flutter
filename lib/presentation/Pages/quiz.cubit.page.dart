import 'package:exo2/business_logic/bloc/quiz.cubit.dart';
import 'package:exo2/business_logic/bloc/theme_cubit.dart';
import 'package:exo2/package data/models/quiz.model.dart';
import 'package:exo2/package%20data/models/theme_model.dart';
import 'package:exo2/presentation/widgets/FloatingButton.dart';
import 'package:exo2/presentation/widgets/Image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class QuizCubitPage extends StatelessWidget with WidgetsBindingObserver {
  const QuizCubitPage({Key? key}) : super(key: key);
  @override
  void didChangePlatformBrightness() {
    super.didChangePlatformBrightness();
    var brightness = SchedulerBinding.instance!.window.platformBrightness;
    bool isDarkMode = brightness == Brightness.dark;
    print("++++++++++++++++++++++++++++++++++++++" + isDarkMode.toString());
  }

  @override
  Widget build(BuildContext context) {
    didChangePlatformBrightness();
    return Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: const Text("Question/réponse"),
          actions: [
            Padding(
              padding: const EdgeInsets.only(right: 20.0),
              child: BlocBuilder<ThemeCubit, ThemeModel>(
                builder: (context, state) => CupertinoSwitch(
                  value: state.switcher,
                  onChanged: (value) {
                    context.read<ThemeCubit>().switchdarkmode(value);
                  },
                ),
              ),
            )
          ],
        ),
        body: BlocBuilder<QuizCubit, quizModel>(
            builder: (context, state) => Center(
                  child: Stack(children: <Widget>[
                    const ImageContainer(),
                    Center(
                        child: Text(
                            context
                                .read<QuizCubit>()
                                .state
                                .getQuestion()[
                                    context.read<QuizCubit>().state.getindex()]
                                .questiontext,
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 20,
                              backgroundColor:
                                  context.read<QuizCubit>().state.getbacktext(),
                            ))),
                    Positioned(
                      top: 450,
                      left: 55,
                      child: RaisedButton(
                        onPressed: context
                                .read<QuizCubit>()
                                .state
                                .getisButtonDisabled()
                            ? null
                            : () => context.read<QuizCubit>().btnVrai(),
                        child: const Text(
                          'Vrai',
                        ),
                      ),
                    ),
                    Positioned(
                      top: 450,
                      left: 155,
                      child: RaisedButton(
                        onPressed: context
                                .read<QuizCubit>()
                                .state
                                .getisButtonDisabled()
                            ? null
                            : () => context.read<QuizCubit>().btnFaux(),
                        child: const Text(
                          'Faux',
                        ),
                      ),
                    ),
                    Positioned(
                      top: 450,
                      left: 255,
                      child: RaisedButton(
                        onPressed: !context
                                .read<QuizCubit>()
                                .state
                                .getisButtonDisabled()
                            ? null
                            : () => {context.read<QuizCubit>().btnSuivant()},
                        child: const Text(
                          '-->',
                        ),
                      ),
                    ),
                    Positioned(
                        child: Text(
                            "Score :  ${context.read<QuizCubit>().state.getscore()}/ ${context.read<QuizCubit>().state.getQuestionCount()}",
                            style: const TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 15,
                            )))
                  ]),
                )),
        floatingActionButton: const FloatingButton());
  }
}
