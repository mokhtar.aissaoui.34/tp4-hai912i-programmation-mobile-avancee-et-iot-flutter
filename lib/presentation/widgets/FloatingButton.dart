// ignore_for_file: file_names

import 'package:exo2/business_logic/bloc/addquestion.cubit.dart';
import 'package:exo2/business_logic/bloc/quiz.cubit.dart';
import 'package:exo2/package%20data/DataProvider/QuestionFirebase.dart';
import 'package:exo2/package%20data/models/question.model.dart';
import 'package:exo2/package%20data/models/quiz.model.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class FloatingButton extends StatelessWidget {
  const FloatingButton({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return FloatingActionButton(
      onPressed: () {
        showDialog(
            context: context,
            builder: (BuildContext context) {
              return AlertDialog(
                content: Stack(
                  children: <Widget>[
                    Positioned(
                      right: -50.0,
                      top: -50.0,
                      child: InkResponse(
                        onTap: () {
                          Navigator.of(context).pop();
                        },
                        child: const CircleAvatar(
                          child: Icon(Icons.close),
                          backgroundColor: Colors.red,
                        ),
                      ),
                    ),
                    BlocBuilder<addquestion, Question>(
                        builder: (context, state) =>
                            BlocBuilder<QuizCubit, quizModel>(
                              builder: (context1, state1) => Form(
                                child: Column(
                                  mainAxisSize: MainAxisSize.min,
                                  children: <Widget>[
                                    Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: TextFormField(
                                        decoration: const InputDecoration(
                                            hintText: "Question :"),
                                        onChanged: (text) {
                                          context
                                              .read<addquestion>()
                                              .updateQuestion(text);
                                        },
                                      ),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: MergeSemantics(
                                        child: ListTile(
                                          title: const Text('Vrai :'),
                                          trailing: CupertinoSwitch(
                                            value: context
                                                .read<addquestion>()
                                                .state
                                                .getIsCo(),
                                            onChanged: (value) {
                                              context
                                                  .read<addquestion>()
                                                  .btnswitch(value);
                                            },
                                          ),
                                        ),
                                      ),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      // ignore: deprecated_member_use
                                      child: RaisedButton(
                                        child: const Text("Submit"),
                                        onPressed: () {
                                          QuestionFirebase.addquestion(
                                              state.questiontext,
                                              state.isCorrct);
                                          ScaffoldMessenger.of(context)
                                              .showSnackBar(const SnackBar(
                                            content:
                                                Text("Question ajoutée..."),
                                          ));
                                          Navigator.of(context).pop();
                                          context
                                              .read<addquestion>()
                                              .restart(context1);
                                        },
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ))
                  ],
                ),
              );
            });
      },
      child: const Icon(Icons.add_comment),
    );
  }
}
