// ignore: file_names
// ignore_for_file: file_names

import 'package:flutter/widgets.dart';

class ImageContainer extends StatelessWidget {
  const ImageContainer({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        height: 600,
        width: 400,
        padding: const EdgeInsets.fromLTRB(50, 30, 50, 30),
        margin: const EdgeInsets.fromLTRB(40, 0, 40, 450),
        decoration: const BoxDecoration(
            image: DecorationImage(
          image: NetworkImage(
              "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSSsuLP2gvaNxSQb1_ivpbKi2IOiNP4FgBLAA&usqp=CAU"),
        )));
  }
}
