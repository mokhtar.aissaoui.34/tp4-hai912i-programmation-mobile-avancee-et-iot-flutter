import 'package:bloc/bloc.dart';
import 'package:exo2/package%20data/models/theme_model.dart';
import 'package:flutter/material.dart';

class ThemeCubit extends Cubit<ThemeModel> {
  ThemeCubit(ThemeModel initialState) : super(initialState);
  void switchdarkmode(bool value) {
    if (value) {
      emit(ThemeModel(
          theme: ThemeData.dark().copyWith(buttonColor: Colors.black),
          switcher: value));
    } else {
      emit(ThemeModel(theme: ThemeData.light(), switcher: value));
    }
  }
}
