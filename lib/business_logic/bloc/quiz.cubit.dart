import 'package:bloc/bloc.dart';
import 'package:exo2/package data/models/quiz.model.dart';
import 'package:exo2/package%20data/DataProvider/QuestionFirebase.dart';
import 'package:flutter/material.dart';

import '../../main.dart';

class QuizCubit extends Cubit<quizModel> {
  QuizCubit(quizModel initialState) : super(initialState);
  void btnVrai() async {
    quizModel f = quizModel(questions: state.getQuestion());
    if (state.getQuestion()[state.getindex()].isCorrct) {
      f.setBackText(Colors.green);
      f.addscore(state.getscore() + 1);
      f.setindex(state.getindex());
      f.setDisable(true);
    } else {
      f.setBackText(Colors.red);
      f.setDisable(true);
      f.addscore(state.getscore());
      f.setindex(state.getindex());
    }
    if (state.getindex() == state.getQuestionCount() - 1) {
      f.viewScore();
    }
    emit(f);
  }

  void btnFaux() async {
    quizModel f = quizModel(questions: state.getQuestion());
    if (!state.getQuestion()[state.getindex()].isCorrct) {
      f.setBackText(Colors.green);
      f.addscore(state.getscore() + 1);
      f.setindex(state.getindex());
      f.setDisable(true);
    } else {
      f.setBackText(Colors.red);
      f.setDisable(true);
      f.addscore(state.getscore());
      f.setindex(state.getindex());
    }
    if (state.getindex() == state.getQuestionCount() - 1) {
      f.viewScore();
    }

    emit(f);
  }

  void btnSuivant() async {
    quizModel f = quizModel(questions: state.getQuestion());
    if (state.getindex() < state.getQuestionCount() - 1) {
      f.setBackText(Colors.transparent);
      f.setindex(state.getindex() + 1);
      f.addscore(state.getscore());
      f.setDisable(false);
    } else {
      f.setBackText(Colors.transparent);
      f.setDisable(false);
      f.setindex(0);
      f.addscore(-f.getscore());
    }
    emit(f);
  }

  void restart() async {
    QuestionFirebase.getAllQuestion()
        .then((value) => {emit(quizModel(questions: value))});
  }
}
