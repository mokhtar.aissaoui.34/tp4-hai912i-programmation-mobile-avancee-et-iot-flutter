import 'package:flutter/material.dart';

class ThemeModel {
  ThemeData theme = ThemeData.light();
  bool switcher;
  ThemeModel({required this.theme, required this.switcher}) {
    if (switcher) {
      theme = ThemeData.dark();
    }
  }
}
