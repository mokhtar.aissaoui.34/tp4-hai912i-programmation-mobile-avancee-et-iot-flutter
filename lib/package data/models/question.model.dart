class Question {
  String questiontext;
  bool isCorrct;
  Question({required this.questiontext, required this.isCorrct});
  bool getIsCo() {
    return isCorrct;
  }

  String getQuestion() {
    return questiontext;
  }

  Question.fromJson(Map<String, dynamic> json)
      : this(
          questiontext: json["questiontext"] as String,
          isCorrct: json["isCorrct"] as bool,
        );

  Map<String, dynamic> toJson() => _questionToJson(this);
  Map<String, dynamic> _questionToJson(Question instance) => <String, dynamic>{
        'questiontext': instance.questiontext,
        'isCorrct': instance.isCorrct,
      };
}
