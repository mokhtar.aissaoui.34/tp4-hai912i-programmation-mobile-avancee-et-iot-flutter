import 'package:exo2/business_logic/bloc/theme_cubit.dart';
import 'package:exo2/package%20data/models/theme_model.dart';
import 'package:exo2/presentation/Pages/quiz.cubit.page.dart';
import 'package:exo2/business_logic/bloc/addquestion.cubit.dart';
import 'package:exo2/business_logic/bloc/quiz.cubit.dart';
import 'package:exo2/package data/models/question.model.dart';
import 'package:exo2/package data/models/quiz.model.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package data/DataProvider/QuestionFirebase.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  try {
    QuestionFirebase.getAllQuestion().then((value) => runApp(MyApp(
          resu: value,
        )));
  } catch (e) {}
}

class MyApp extends StatefulWidget {
  List<Question> resu;
  MyApp({
    Key? key,
    required this.resu,
  }) : super(key: key);

  @override
  State<MyApp> createState() => _MyAppState(resu);
}

class _MyAppState extends State<MyApp> with WidgetsBindingObserver {
  List<Question> resu;
  _MyAppState(this.resu);
  @override
  void didChangePlatformBrightness() {
    super.didChangePlatformBrightness();
    var brightness = SchedulerBinding.instance!.window.platformBrightness;
    bool isDarkMode = brightness == Brightness.dark;
    print(isDarkMode);
  }

  @override
  Widget build(BuildContext context) {
    var brightness = SchedulerBinding.instance!.window.platformBrightness;
    bool isDarkMode = brightness == Brightness.dark;
    return MultiBlocProvider(
        providers: [
          BlocProvider(
              create: (context) => QuizCubit(quizModel(questions: resu))),
          BlocProvider(
            create: (context) => addquestion(Question(
              questiontext: "",
              isCorrct: false,
            )),
          ),
          BlocProvider(
              create: (context) => ThemeCubit(
                  ThemeModel(theme: ThemeData.light(), switcher: isDarkMode))),
        ],
        child: BlocBuilder<ThemeCubit, ThemeModel>(
          builder: (context, state) => MaterialApp(
            title: 'Flutter Demo',
            theme: state.theme,
            darkTheme: state.theme,
            home: const QuizCubitPage(),
          ),
        ));
  }
}
